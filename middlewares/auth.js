const jwt = require('jsonwebtoken')
const fs = require('fs')
const Sauce = require('../models/sauce')

module.exports = (req, res, next) => {
	try {
		const token = req.headers.authorization.split(' ')[1]
		const decodedToken = jwt.verify(token, 'HugVomRoutsyuthosiJebIWyxRifiad5')
		userId = decodedToken.userId
		if (req.file) {
			const sauce = JSON.parse(req.body.sauce)
			if (sauce.userId !== userId)
				throw new Error('Invalid user ID.')
		}
		else if (req.body.userId) {
			if (req.body.userId !== userId)
				throw new Error('Invalid user ID.')
		}
		else if (req.params.id) {
			Sauce.findOne({_id: req.params.id})
			.then (sauce => {
				if (!sauce)
					throw new Error('Invalid sauce.')
				if (sauce.userId !== userId)
					throw new Error('Invalid user ID.')
			})
			.catch (() => res.status(500).send({message: 'Server Error.'}))
		}
		next()
	}
	catch (err) {
		if (req.file)
			fs.unlinkSync(req.file.path)
		res.status(403).send({message: "403: unauthorized request"})
	}
}
