const express = require('express')

const sauceCtrl = require('../controllers/sauces')
const auth = require('../middlewares/auth')
const multer = require('../middlewares/multer-config')

const router = express.Router()

router.get('/', sauceCtrl.getAllSauces)
router.get('/:id', sauceCtrl.getOneSauce)
router.post('/', multer, auth, sauceCtrl.createSauce)
router.post('/:id/like', auth, sauceCtrl.likeSauce)
router.put('/:id', multer, auth, sauceCtrl.modifySauce)
router.delete('/:id', auth, sauceCtrl.deleteSauce)

module.exports = router
