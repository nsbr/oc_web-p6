const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const User = require('../models/user')

exports.signup = (req, res, next) => {
	bcrypt.hash(req.body.password, 10)
	.then (hash => {
		const user = new User({
			email: req.body.email,
			password: hash
		})
		user.save()
		.then(() => res.status(201).json({ message: 'User created.' }))
		.catch (err => { 
			if ( err.name === 'ValidationError')
				res.status(400).json({ message: 'E-mail already in use.' })
			else
				res.status(500).json({ message: 'Server Error.' })
		})
	})
	.catch (err => res.status(500).json({message: 'Server Error.'}))
}


exports.login = (req, res, next) => {
	User.findOne({ email: req.body.email })
	.then (user => {
		if (!user)
			return res.status(401).send()
		bcrypt.compare(req.body.password, user.password)
		.then (valid => {
			if (!valid)
				return res.status(401).send()
			res.status(200).json({
				userId: user._id,
				token: jwt.sign(
					{ userId: user._id},
					'HugVomRoutsyuthosiJebIWyxRifiad5',
					{ expiresIn: '24h' }
				)
			})
		})
		.catch(err => res.status(401).send())
	})
	.catch(err => res.status(500).json({message: 'Server Error.'}))
}
