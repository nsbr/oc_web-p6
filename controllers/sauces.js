const Sauce = require('../models/sauce')
const fs = require('fs')
const path = require('path')    

exports.getAllSauces = (req, res, next) => {
	Sauce.find()
	.then ((sauces) => {
	if (!sauces.length)
		res.status(500).json({message: "No sauces." })
	else
		res.status(200).json(sauces)
	})
	.catch (() => res.status(500).send({message: 'Server Error.'}))
	
}

exports.getOneSauce = (req, res, next) => {
	Sauce.findOne({_id: req.params.id})
	.then (sauce => {
	if (!sauce)
		res.status(400).json({message: "No sauce." })
	res.status(200).json(sauce)
	})
	.catch (() => res.status(500).send({message: 'Server Error.'}))
}

exports.createSauce = (req, res, next) => {
	const sauce = new Sauce({
		...JSON.parse(req.body.sauce),
		imageUrl:  `${req.protocol}://${req.get('host')}/${req.file.path}`,
		likes:  0,
		dislikes: 0,
		usersLiked: [],
		usersDisliked: []
	})
	sauce.save()
	.then(() => res.status(201).json({ message: 'Sauce added.' }))
	.catch (() => {
		fs.unlinkSync(req.file.path)
		res.status(500).json({ message: 'Couldn\'t add the sauce.' })
	})
}

exports.likeSauce = (req, res, next) => {
	Sauce.findOne({_id: req.params.id})
	.then (sauce => {
		if (!sauce) {
			 res.status(400).json({message: "Sauce not found." })
			 return
		}
		let mod = false
		switch (req.body.like) {
		case 1:
			if (!sauce.usersLiked.includes(req.body.userId)) {
				sauce.usersLiked.push(req.body.userId)
				mod = true
			}
			if ((i = sauce.usersDisliked.indexOf(req.body.userId)) !== -1) {
				sauce.usersDisliked.splice(i, 1)
				mod = true
			}
			break
		case -1:
			if (!sauce.usersDisliked.includes(req.body.userId)) {
				sauce.usersDisliked.push(req.body.userId)
				mod = true
			}
			if ((i = sauce.usersLiked.indexOf(req.body.userId)) !== -1) {
				sauce.usersLiked.splice(i, 1)
				mod = true
			}
			break
		case 0:
			if ((i = sauce.usersLiked.indexOf(req.body.userId)) !== -1) {
				sauce.usersLiked.splice(i, 1)
				mod = true
			}
			if ((i = sauce.usersDisliked.indexOf(req.body.userId)) !== -1) {
				sauce.usersDisliked.splice(i, 1)
				mod = true
			}
			break
		}
		sauce.likes = sauce.usersLiked.length
		sauce.dislikes = sauce.usersDisliked.length
		if (mod)
			Sauce.updateOne({_id: req.params.id}, sauce)
			.then(() => res.status(200).json({ message: 'Sauce modified.' }))
			.catch (() => {
				fs.unlinkSync(req.file.path)
				res.status(500).json({ message: 'Couldn\'t modify the sauce.' })
			})
	})
	.catch (() => res.status(500).send({message: 'Server Error.'}))
}

const deleteImage = sauce => {
	// compatible with unix paths and windows paths
	const url = sauce.imageUrl.split(path.sep)
	const filename = url.pop()
	const dir = url.join('/').split('/').slice(-1)[0]
	fs.unlinkSync(dir + path.sep + filename)
}

exports.modifySauce = (req, res, next) => {
	if (req.file) {
		sauceMod = {
			...JSON.parse(req.body.sauce),
			imageUrl:  `${req.protocol}://${req.get('host')}/${req.file.path}`,
		}
		Sauce.findOne({ _id: req.params.id })
		.then(sauce => deleteImage(sauce))
		.catch (() => res.status(500).json({ message: 'Internal Error' }))
	}
	else {
		sauceMod = { ...req.body }
	}
	Sauce.updateOne({ _id: req.params.id }, { ...sauceMod })
	.then(() => res.status(200).json({ message: 'Sauce modified.' }))
	.catch (() => res.status(500).json({ message: 'Couldn\'t modify the sauce.' }))
}

exports.deleteSauce = (req, res, next) => {
	Sauce.findOne({ _id: req.params.id })
	.then(sauce => {
		Sauce.deleteOne({ _id: req.params.id })
		.then (() => {
			deleteImage(sauce)
			res.status(200).json({ message: 'Sauce deleted.' })
		})
		.catch (() => res.status(500).json({ message: 'Couldn\'t delete the sauce.' }))
	})
	.catch (() => res.status(500).json({ message: 'Couldn\'t delete the sauce.' }))
}
