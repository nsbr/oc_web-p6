const express = require('express')
const mongoose = require('mongoose')

const saucesRoutes = require('./routes/sauces')
const authRoutes = require('./routes/auth')
const errorHandler = require('./middlewares/error-handler')

const app = express()

// connect to the database
mongoose.connect('mongodb+srv://user-8031:flpleOlPHoD6ZgdG@cluster0.ljt0s.mongodb.net/myDatabase?retryWrites=true&w=majority',
	{ useNewUrlParser: true,
	useUnifiedTopology: true })
	.catch (err => console.log(err))

// set HTTP headers
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content, Accept, Content-Type, Authorization')
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTION')
	next()
})

// parse request body
app.use(express.json())

// link images route to server images folder
app.use('/images', express.static('images'))

app.use('/api/sauces', saucesRoutes)
app.use('/api/auth', authRoutes)

app.use(errorHandler)

module.exports = app
