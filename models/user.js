const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const userSchema = mongoose.Schema ({
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
})

userSchema.plugin(uniqueValidator)

userSchema.post('save', function(error, doc, next) {
	console.log(error)
	next()
})

userSchema.post('update', function(error, doc, next) {
	console.log(error)
	next()
})

module.exports = mongoose.model('User', userSchema)
