const mongoose = require('mongoose')

const sauceSchema = mongoose.Schema ({
	userId: { type: String, required: true },
	name: { type: String, required: true },
	manufacturer: { type: String, required: true },
	description: { type: String, required: true },
	mainPepper: { type: String, required: true },
	imageUrl: { type: String, required: true },
	heat: { type: Number, required: true },
	likes: { type: Number, required: true },
	dislikes: { type: String, required: true },
	usersLiked: { type: Array, required: true },
	usersDisliked: { type: Array, required: true }
})

sauceSchema.post('save', function(error, doc, next) {
	console.log(error)
	next()
})

sauceSchema.post('update', function(error, doc, next) {
	console.log(error)
	next()
})

module.exports = mongoose.model('Sauce', sauceSchema)
